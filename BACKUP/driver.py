#!/usr/bin/env python3
# NeoPixel library strandtest example
# Author: Tony DiCola (tony@tonydicola.com)
#
# Direct port of the Arduino NeoPixel library strandtest example.  Showcases
# various animations on a strip of NeoPixels.

import time
from neopixel import *
import argparse
import RPi.GPIO as GPIO
import random

GPIO.setmode(GPIO.BCM) # use GPIO numbers instead of pin numbers

# inputs on GPIO  (MSB) 5, 6, 13, 19, 26 (LSB)
GPIO.setup(5,GPIO.IN,pull_up_down=GPIO.PUD_UP)
GPIO.setup(6,GPIO.IN,pull_up_down=GPIO.PUD_UP)
GPIO.setup(13,GPIO.IN,pull_up_down=GPIO.PUD_UP)
GPIO.setup(19,GPIO.IN,pull_up_down=GPIO.PUD_UP)
GPIO.setup(26,GPIO.IN,pull_up_down=GPIO.PUD_UP)

# LED strip configuration:
LED_COUNT      = 60      # Number of LED pixels.
LED_PIN        = 18      # GPIO pin connected to the pixels (18 uses PWM!).
#LED_PIN        = 10      # GPIO pin connected to the pixels (10 uses SPI /dev/spidev0.0).
LED_FREQ_HZ    = 800000  # LED signal frequency in hertz (usually 800khz)
LED_DMA        = 10      # DMA channel to use for generating signal (try 10)
LED_BRIGHTNESS = 255     # Set to 0 for darkest and 255 for brightest
LED_INVERT     = False   # True to invert the signal (when using NPN transistor level shift)
LED_CHANNEL    = 0       # set to '1' for GPIOs 13, 19, 41, 45 or 53


def LEDon(strip,index,col):
  strip.setPixelColor(index,col)
  strip.show()
  time.sleep(.05)

# Define functions which animate LEDs in various ways.
def colorWipe(strip, color, wait_ms=50):
    """Wipe color across display a pixel at a time."""
    for i in range(strip.numPixels()):
        strip.setPixelColor(i, color)
        strip.show()
        time.sleep(wait_ms/1000.0)

# Main program logic follows:
if __name__ == '__main__':
    # Process arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--clear', action='store_true', help='clear the display on exit')
    args = parser.parse_args()

    # Create NeoPixel object with appropriate configuration.
    strip = Adafruit_NeoPixel(LED_COUNT, LED_PIN, LED_FREQ_HZ, LED_DMA, LED_INVERT, LED_BRIGHTNESS, LED_CHANNEL)
    # Intialize the library (must be called once before other functions).
    strip.begin()

    print ('Press Ctrl-C to quit.')
    if not args.clear:
        print('Use "-c" argument to clear LEDs on exit')

    try:
        lastLED=0 # last LED turned on

        red=128
        green=255
        blue=0

        while True:
          led=0
          if GPIO.input(26) == 1:
            led=led+1
          if GPIO.input(19) == 1:
            led=led+2
          if GPIO.input(13) == 1:
            led=led+4
          if GPIO.input(6) == 1:
            led=led+8
          if GPIO.input(5) == 1:
            led=led+16
# etc...led is new index

          if led != lastLED:
            LEDon(strip,lastLED,0) # turn off old light
            LEDon(strip,led,Color(red,green,blue)) # order is wrong

          red+=random.randint(-8,8)
          green+=random.randint(-8,8)
          blue+=random.randint(-8,8)
          red=(red+256)%256
          green=(green+256)%256
          blue=(blue+256)%256
          lastLED=led

    except KeyboardInterrupt:
        if args.clear:
            colorWipe(strip, Color(0,0,0), 10)
