#include <stdio.h>
#include <stdlib.h>
#include <wiringPi.h>
#include "includeme.h"

#define OFFSET 30 // subtract from note index

//
// main driver for FFT fun :)
// do the initializations as usual
// Then input an audio buffer fomr ALSA, covert to freq domain,
// find peaks, find correspondi ng notes, and then
// print (for now) or drive LEDs (eventually!)
//

void sendInit(); // initialize LED communication
void send(int); // light an LED

int main()
{
  double *buffer;
  int freqs[500];
  int fcount[500];

  buffer=malloc(N*sizeof(double)); // store (single-channel) audio input here
  for (int i=0;i<500;i++) fcount[i]=0; // count # of consecutive occurrances :)

  if (inputInit()) return(1);
  if (fftInit()) return(1);

  sendInit(); // set GPIOs as outputs

  while (1){ // forevah!
    inputIn(buffer);
//printf("Input done\n");
    int size=fftCvt(buffer,freqs);
//printf("fftcvt returned %d\n",size);
    if (size==0) continue; // no notes!
    for (int i=0;i<size;i++){
      if (freqs[i]!=0){
        ++fcount[i];
      } else {
        fcount[i]=0; // RESET!!!
      }
    }

// dump notes
    int output=0; // set when we do output
    for (int i=0;i<size;i++){
      //if (fcount[i]==1){
      if (fcount[i]>0){
        //printf("%d %s ",i,noteName(i)); // only once ;)
        printf("%s ",noteName(i)); // only once ;)
        output=1;
/***
        printf("Sending %d\n",i);
        if (i >= OFFSET){
          send(i-OFFSET);
        }
***/
        send(i%12);
      }
    }
    fflush(stdout);
    //if (output) printf("\n");
  }
}

// code to send 5-bit number to LED driver via GPIOs

#define b0 25 // LSb WiringPi numbers. See gpio readall
#define b1 24
#define b2 23
#define b3 22
#define b4 21 // MSb

void sendInit() // call once
{
  wiringPiSetup(); // setup system

  pinMode(b0,OUTPUT);
  pinMode(b1,OUTPUT);
  pinMode(b2,OUTPUT);
  pinMode(b3,OUTPUT);
  pinMode(b4,OUTPUT);
}

void send(int i) // call to send an integer
{
  digitalWrite(b0,(i&1)?HIGH:LOW);
  digitalWrite(b1,(i&2)?HIGH:LOW);
  digitalWrite(b2,(i&4)?HIGH:LOW);
  digitalWrite(b3,(i&8)?HIGH:LOW);
  digitalWrite(b4,(i&16)?HIGH:LOW);
}
