#include <stdio.h>
#include <fftw3.h>
#include <math.h>
#include <string.h>
#include "includeme.h"

void load0(fftw_complex *in); // zero-out the input array (testing only)
void load(fftw_complex *in,double freq); // sum a sine wave of the given freq (testing only)

void freqInit(); // load notes[] array
int findNote(double freq); // find index in notes[] array

struct note{
  char name[4];
  int index; // 0=A1
  double freq;
};

static struct note notes[500]; // array of frequencies
static int notesSize=0; // # meaningful element in notes[] array

static fftw_complex *in, *out;
static fftw_plan p;

int fftInit()
{

  freqInit(); // set up array of frequencies

  in = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * N);
  out = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * N);
  p = fftw_plan_dft_1d(N, in, out, FFTW_FORWARD, FFTW_ESTIMATE);
  return(0);
}

int fftCvt(double *data, int *freqs)
{
  for (int i=0;i<N;i++){
    in[i][0]=data[i];
    in[i][1]=0; // real-part only
  }

  fftw_execute(p); // DO IT!

// find max
  double max=0,this;
  for (int i=0;i<N/2;i++){
  //for (int i=0;i<N;i++){
    //this=fabs(out[i][0])+fabs(out[i][1]);
    this=out[i][0]=sqrt(out[i][0]*out[i][i] + out[i][1]*out[i][1]); // save it!!!
    if (this > max){
      max=this;
    }
  }
  //printf("Raw max=%f\n",max); // 500 seems like a decent noise threshold
  // ??? if (max < NOISE_THRESHOLD) max=NOISE_THRESHOLD; // :)
  //max=max*.90; // anything above here is considered a peak

// prepare output array
  for (int i=0;i<notesSize;i++) {freqs[i]=0;}

// find peaks
  double maxlocs[10];int nummax=0; // location and # of peaks
  for (int i=0;i<N/2;i++){
  //for (int i=0;i<N;i++){
    this=out[i][0]; // saved above :)

    if (this >= max){
      maxlocs[nummax]=((double)i)/PERIOD;
      if (nummax < 9) ++nummax; // only keep 10
    }
  }

  //printf("peaks: ");
  //for (int i=0;i<nummax;i++) printf("%d ",maxlocs[i]);
  int retVal=0; // no note!
  for (int i=0;i<nummax;i++){
    int noteIndex=findNote((double) maxlocs[i]); // find closest note
    //printf("%d HZ, ind=%d (%s)  ",maxlocs[i],notes[noteIndex].index,notes[noteIndex].name);
    if (notes[noteIndex].index > 1){ // A1 and Bb1 (?)
      freqs[(notes[noteIndex].index)]=1; // flag it // WRAP TO 12 NOTES! // no...
      retVal=notesSize; // tell caller we have data!
    }
// pass this back eventually
  }
  //printf("\n");

  return(retVal);
}

int fftDeinit()
{
  fftw_destroy_plan(p);
  fftw_free(in); fftw_free(out);
  return(0);
}

// zero-out input array
void load0(fftw_complex *in)
{
  for (int i=0;i<N;i++){
    in[i][0]=in[i][1]=0;
  }
}

void load(fftw_complex *in,double freq)
{
  for (int i=0;i<N;i++){
    double x=i;
    double t=(x/N)*PERIOD;
    in[i][0]+=sin(freq*6.28*t); // sum these!
//    in[i][1]=0; // real only?
  }
}

void freqInit()
{
  double f=55; // low A
  double scale=pow(2,1./12); // mutiply by this
  int octave=1;
  int toneNum=1;
  int notesIndex=0; // save in notes[this]

  char name[4];
  while (octave < 9){
    notes[notesIndex].freq=f; // save frequency
    notes[notesIndex].index=notesIndex; // save ordered freq code (for driving LEDs)

// build note name
    switch(toneNum++){
      case  1:sprintf(name,"A%d",octave);break;
      case  2:sprintf(name,"Bb%d",octave);break;
      case  3:sprintf(name,"B%d",octave);break;
      case  4:sprintf(name,"C%d",octave);break;
      case  5:sprintf(name,"C#%d",octave);break;
      case  6:sprintf(name,"D%d",octave);break;
      case  7:sprintf(name,"Eb%d",octave);break;
      case  8:sprintf(name,"E%d",octave);break;
      case  9:sprintf(name,"F%d",octave);break;
      case 10:sprintf(name,"F#%d",octave);break;
      case 11:sprintf(name,"G%d",octave);break;
      case 12:sprintf(name,"Ab%d",octave);
              // and move to next octave
              ++octave;
              toneNum=1; // back to A
              break;
      default:printf("ERROR! toneNum=%d\n",toneNum);return;
    }
    strcpy(notes[notesIndex].name,name);
    ++notesIndex;
    f*=scale;
  }
  notesSize=notesIndex;
}

char *noteName(int ind) // return note name
{
  return(notes[ind].name);
}

int findNote(double freq)
{
  int low=0;
  int high=notesSize-1;
  int mid;

  while (low <= high){
    mid=(low+high)/2;
    if (freq > notes[mid].freq){ // too low
      low=mid+1;
    } else {
      high=mid-1;
    }
  }
// answer is somewhere around mid
  if (mid==0) mid=1; // hahaha
  if (mid==notesSize-2) --mid; // HOHOHO

// pick closest bucket
  double d1=fabs(freq-notes[mid-1].freq);
  double d2=fabs(freq-notes[mid].freq);
  double d3=fabs(freq-notes[mid+1].freq);
  if ((d1 < d2) && (d1 < d3)) return(mid-1);
  if ((d2 < d1) && (d2 < d3)) return(mid);
  return(mid+1); // return closest frequency

// linear search for now...
  for (int i=0;i<notesSize;i++){
    if (notes[i].freq > freq){
      return(i);
    }
  }
// ooops!
  printf("ERROR! Didn't find index for freq=%f\n",freq);
  return(0);
}
