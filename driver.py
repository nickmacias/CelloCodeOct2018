#!/usr/bin/env python3
# NeoPixel library strandtest example
# Original animation code by Tony DiCola (tony@tonydicola.com)
#
# Mod by N Macias - GPIO monitor, note display, animation change on
#                   note change, etc. 
#
# Direct port of the Arduino NeoPixel library strandtest example.  Showcases
# various animations on a strip of NeoPixels.

import time
from neopixel import *
import argparse
import RPi.GPIO as GPIO
import random

# LED strip configuration:
REAL_LED_COUNT=120     # Number of (non-fingerboard) LED pixels.
LED_PIN=18            # GPIO pin connected to the pixels (18 uses PWM!).
#LED_PIN=10           # GPIO pin connected to the pixels (10 uses SPI /dev/spidev0.0).
LED_FREQ_HZ=800000    # LED signal frequency in hertz (usually 800khz)
LED_DMA=10            # DMA channel to use for generating signal (try 10)
LED_BRIGHTNESS=255    # Set to 0 for darkest and 255 for brightest
LED_INVERT=False      # True to invert the signal (when using NPN transistor level shift)
LED_CHANNEL=0         # set to '1' for GPIOs 13, 19, 41, 45 or 53

LED_COUNT=0 # set to REAL_LED_COUNT or REAL_LED_COUNT/2

LEDOFFSET=12 # start animations from here

# globals for note display
lastLED=0 # last LED turned on
NEWLED=0   # set when an LED has changed
DORAPIDCHANGE=1   # set to 1 to cause animations to change every note
DOREFLECTION=0 # set to do left/right reflection
FULLNOTE=0   # 0=A, 11=G#  Light all LEDs on this note :)

# randomized color components
red=128
green=255
blue=0

# GPIO setup and initialization
def GPIOSetup():
  GPIO.setmode(GPIO.BCM) # use GPIO numbers instead of pin numbers

# LED setup
def LEDSetup():
  global LED_COUNT # Need to change this!

  if (DOREFLECTION==1):
    LED_COUNT=REAL_LED_COUNT/2
  else:
    LED_COUNT=REAL_LED_COUNT

# inputs on GPIO  (MSB) 5, 6, 13, 19, 26 (LSB)
  GPIO.setup(5,GPIO.IN,pull_up_down=GPIO.PUD_UP) 
  GPIO.setup(6,GPIO.IN,pull_up_down=GPIO.PUD_UP)
  GPIO.setup(13,GPIO.IN,pull_up_down=GPIO.PUD_UP)
  GPIO.setup(19,GPIO.IN,pull_up_down=GPIO.PUD_UP) 
  GPIO.setup(26,GPIO.IN,pull_up_down=GPIO.PUD_UP)

# set a single LED
def LEDon(strip,index,col):
  strip.setPixelColor(index,col)
  strip.show()
  time.sleep(.01)

# Define functions which animate LEDs in various ways.
def colorWipe(strip, color, wait_ms=50):
    global NEWLED
    """Wipe color across display a pixel at a time."""
    for i in range(strip.numPixels()):
        if (NEWLED == 1):
          NEWLED=0
          return
        setPixelColorOff(i, color)
        strip.show()
        time.sleep(wait_ms/1000.0)

def theaterChase(strip, color, wait_ms=50, iterations=10):
    global NEWLED
    """Movie theater light style chaser animation."""
    for j in range(iterations):
        for q in range(3):
            for i in range(0, strip.numPixels(), 3):
                setPixelColorOff(i+q, color)
                if (NEWLED == 1):
                  NEWLED=0
                  return
            strip.show()
            time.sleep(wait_ms/1000.0)
            for i in range(0, strip.numPixels(), 3):
                setPixelColorOff(i+q, 0)

def wheel(pos):
    """Generate rainbow colors across 0-255 positions."""
    if pos < 85:
        return Color(pos * 3, 255 - pos * 3, 0)
    elif pos < 170:
        pos -= 85
        return Color(255 - pos * 3, 0, pos * 3)
    else:
        pos -= 170
        return Color(0, pos * 3, 255 - pos * 3)

def rainbow(strip, wait_ms=20, iterations=1):
    """Draw rainbow that fades across all pixels at once."""
    global NEWLED
    for j in range(256*iterations):
        for i in range(strip.numPixels()):
            setPixelColorOff(i, wheel((i+j) & 255))
            if (NEWLED == 1):
              NEWLED=0
              return
        strip.show()
        time.sleep(wait_ms/1000.0)

def rainbowCycle(strip, wait_ms=20, iterations=5):
    """Draw rainbow that uniformly distributes itself across all pixels."""
    global NEWLED
    for j in range(256*iterations):
        for i in range(strip.numPixels()):
            setPixelColorOff(i, wheel((int(i * 256 / strip.numPixels()) + j) & 255))
            if (NEWLED == 1):
              NEWLED=0
              return
        strip.show()
        time.sleep(wait_ms/1000.0)

def theaterChaseRainbow(strip, wait_ms=50):
    """Rainbow movie theater light style chaser animation."""
    global NEWLED
    for j in range(256):
        for q in range(3):
            for i in range(0, strip.numPixels(), 3):
                setPixelColorOff(i+q, wheel((i+j) % 255))
                if (NEWLED == 1):
                  NEWLED=0
                  return
            strip.show()
            time.sleep(wait_ms/1000.0)
            for i in range(0, strip.numPixels(), 3):
                setPixelColorOff(i+q, 0)
                if (NEWLED == 1):
                  NEWLED=0
                  return

# local setPixelColor with built-in offset
def setPixelColorOff(i,color):
  #print 'ledcount=',LED_COUNT,' real=',REAL_LED_COUNT,' i=',i
  if ((DOREFLECTION==1) and (i>LED_COUNT)): # only do half!
    return
  strip.setPixelColor(i+LEDOFFSET,color)
  if (DOREFLECTION == 1):
    strip.setPixelColor((REAL_LED_COUNT-i)+LEDOFFSET,color)
  checkNote()  # look for new note and light LED

def checkNote():
  global lastLED,NEWLED,red,green,blue

  #print "Note check"
  led=0
  if GPIO.input(26) == 1:
    led=led+1
  if GPIO.input(19) == 1:
    led=led+2
  if GPIO.input(13) == 1:
    led=led+4
  if GPIO.input(6) == 1:
    led=led+8
  if GPIO.input(5) == 1:
    led=led+16
# etc...led is new index

##%%% Adjust this!
  led=led%12
  #print "led="
  #print led
  if led != lastLED:
    NEWLED=DORAPIDCHANGE  # flag main LEDs to change animation
    if (lastLED==FULLNOTE):
      for i in range (0,12):
        LEDon(strip,i,0)
    LEDon(strip,lastLED,0) # turn off old light

    if (led==FULLNOTE):
      for i in range (0,12):
        LEDon(strip,i,Color(red,green,blue))
    LEDon(strip,led,Color(red,green,blue)) # order is wrong

  red+=random.randint(-8,8)
  green+=random.randint(-8,8)
  blue+=random.randint(-8,8)
  red=(red+256)%256
  green=(green+256)%256
  blue=(blue+256)%256
  lastLED=led


# Main program logic follows:
if __name__ == '__main__':
    # Process arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--clear', action='store_true', help='clear the display on exit')
    args = parser.parse_args()

    # Create NeoPixel object with appropriate configuration.
    strip = Adafruit_NeoPixel(REAL_LED_COUNT+LEDOFFSET, LED_PIN, LED_FREQ_HZ, LED_DMA, LED_INVERT, LED_BRIGHTNESS, LED_CHANNEL)
    # Intialize the library (must be called once before other functions).
    strip.begin()

    print ('Press Ctrl-C to quit.')
    if not args.clear:
        print('Use "-c" argument to clear LEDs on exit')

    GPIOSetup(); # :)
    LEDSetup();

    try:

# animations use the offset pixel code (setPixelColorOff)
# which (a) adds an offset, to skip over the fingerboard; and
# (b) calls checkNote to see if the fingerboard LEDs need updating

        while True:
            print ('Color wipe animations.')
            colorWipe(strip, Color(255, 0, 0))  # Red wipe
            theaterChase(strip, Color(  0,   0, 127))  # Blue theater chase
            rainbow(strip)
            colorWipe(strip, Color(0, 0, 255))  # Green wipe
            print ('Theater chase animations.')
            theaterChase(strip, Color(127, 127, 127))  # White theater chase
            rainbowCycle(strip,2)
            colorWipe(strip, Color(0, 255, 0))  # Blue wipe
            theaterChase(strip, Color(127,   0,   0))  # Red theater chase
            #print ('Rainbow animations.')
            theaterChaseRainbow(strip)

    except KeyboardInterrupt:
        if args.clear:
            colorWipe(strip, Color(0,0,0), 10)
