int inputInit(); // prepare to read audio data
int inputIn(double *buffer); // read a buffer of doubles (mono)
int inputDeinit(); // close out audio system
int fftInit(); // prepare for FFT conversions
int fftCvt(double *data, int *freqs); // convert a time-domain buffer to a frequency array
                                      // returns size of freq array
int fftDeinit(); // release resources

char *noteName(int ind); // return short string with note's name

// PERIOD=.01, N=204 for smaller sample size
// PERIOD=.1, N=2048 for 10 Hz min
// PERIOD=.025, N=512 for 40 Hz min
//#define PERIOD 0.05 // time of sample (4/100th sec for now)
#define PERIOD (2205./44100.) // 1024 samples out of 44100
#define N 2205       // sample size (good up to approx 10KHz)
                   // so need to sample 20000x per sec
                  // sampling time=49 uSec
#define NOISE_THRESHOLD 30
