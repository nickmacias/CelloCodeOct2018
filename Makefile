main: main.o fft.o input.o
	gcc -g -o main main.o fft.o input.o -lfftw3 -lasound -lm
main.o: main.c includeme.h
	gcc -g -c main.c
fft.o: fft.c includeme.h
	gcc -g -c fft.c
input.o: input.c includeme.h
	gcc -g -c input.c
